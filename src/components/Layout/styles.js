import styled from 'styled-components';

export const Div = styled.div`
  padding: 60px 16px 0;
`;

export const Title = styled.h1`
  font-size: 24px;
  font-weight: 600;
  color: #222;
  padding-bottom: 8px;
`;

export const Subtitle = styled.h2`
  font-size: 15px;
  font-weight: 400;
  color: #333;
  padding-bottom: 4px;
`;
