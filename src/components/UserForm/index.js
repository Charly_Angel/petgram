import React from 'react';
import PropTypes from 'prop-types';
import { useInputValue } from '../../hooks/useInputValue';
import { SubmitButton } from '../SubmitButton';
import { Form, Input, Error } from './styles';

export const UserForm = ({ onSubmit, title, error, disabled }) => {
  const email = useInputValue('');
  const password = useInputValue('');

  const handleSubmit = (e) => {
    e.preventDefault();
    onSubmit({ email: email.value, password: password.value });
  };

  return (
    <>
      <Form onSubmit={handleSubmit} disabled={disabled}>
        <Input
          type='email'
          placeholder='Email'
          name='email'
          {...email}
          autocomplete='off'
          disabled={disabled}
        />
        <Input
          type='password'
          placeholder='Password'
          name='password'
          autocomplete='off'
          {...password}
          disabled={disabled}
        />
        <SubmitButton disabled={disabled}>{title}</SubmitButton>
      </Form>
      {error && <Error>{error}</Error>}
    </>
  );
};

UserForm.propTypes = {
  onSubmit: PropTypes.func.isRequired,
  title: PropTypes.string,
  error: PropTypes.string,
  disabled: PropTypes.bool,
};
