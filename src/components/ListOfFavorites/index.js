import React from 'react';
import PropTypes from 'prop-types';
import { Link, Grid, Image } from './styles';

export const ListOfFavorites = ({ favorites = [] }) => {
  return (
    <Grid>
      {favorites.map((fav) => (
        <Link key={fav.id} to={`/detail/${fav.id}`}>
          <Image src={fav.src} />
        </Link>
      ))}
    </Grid>
  );
};

ListOfFavorites.propTypes = {
  favorites: PropTypes.arrayOf(
    PropTypes.shape({
      id: PropTypes.string.isRequired,
      src: PropTypes.string.isRequired,
    }),
  ),
};
