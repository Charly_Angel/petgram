import React from 'react';
import { Logo } from '../Logo';
import { Nav } from './styles';

export const Header = () => {
  return (
    <Nav>
      <Logo />
    </Nav>
  );
};
