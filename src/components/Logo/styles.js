import styled from 'styled-components';

export const Svg = styled.svg`
  width: 200px;
  margin-left: -5px;
  margin-right: -15px;
`;
