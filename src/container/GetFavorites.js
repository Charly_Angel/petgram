import React from 'react';
import { Query } from '@apollo/client/react/components';
import { gql } from '@apollo/client';
import { ListOfFavorites } from '../components/ListOfFavorites';

const GET_FAVORITES = gql`
  query getFavs {
    favs {
      id
      categoryId
      src
      likes
      userId
    }
  }
`;

const renderProp = ({ loading, error, data }) => {
  if (loading) return <p>Loading...</p>;
  if (error) return <p>Error!</p>;

  const { favs } = data || { favs: [] };
  return <ListOfFavorites favorites={favs} />;
};

export const FavoritesWithQuery = () => {
  return (
    <Query query={GET_FAVORITES} fetchPolicy='network-only'>
      {renderProp}
    </Query>
  );
};
