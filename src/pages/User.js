import React, { useContext } from 'react';
import { SubmitButton } from '../components/SubmitButton';
import { Context } from '../Context';
import { Layout } from '../components/Layout';

export default () => {
  const { removeAuth } = useContext(Context);

  return (
    <Layout title='Perfil de usuario'>
      <SubmitButton onClick={removeAuth}>Cerrar sesión</SubmitButton>
    </Layout>
  );
};
