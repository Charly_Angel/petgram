import React from 'react';

import UserAuthenticated from './UserAuthenticated';
import NotUserAuthenticated from './NotUserAuthenticated';

export default function AppRouter({ isAuth }) {
  return isAuth ? (
    <UserAuthenticated isAuth={isAuth} />
  ) : (
    <NotUserAuthenticated isAuth={isAuth} />
  );
}
