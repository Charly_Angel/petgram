import React from 'react';
import { Router, Redirect } from '@reach/router';

import { Login } from '../pages/Login';
import { SignUp } from '../pages/SignUp';
import { NotFound } from '../pages/NotFound';

export default function NotUserAuthenticated({ isAuth }) {
  return (
    <Router>
      <NotFound default />
      <Login path='/login' />
      <SignUp path='/signup' />
      {!isAuth && <Redirect noThrow from='/' to='/login' />}
      {!isAuth && <Redirect noThrow from='/favorites' to='/login' />}
      {!isAuth && <Redirect noThrow from='/user' to='/login' />}
      {!isAuth && <Redirect noThrow from='/pet' to='/login' />}
      {!isAuth && <Redirect noThrow from='/detail' to='/login' />}
    </Router>
  );
}
