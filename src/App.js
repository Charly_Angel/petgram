import React, { useContext, Suspense } from 'react';

import AppRouter from './routes/AppRouter';
import { GlobalStyle } from './styles/GlobalStyles';
import { Header } from './components/Header';
import { Navbar } from './components/Navbar';
import { Context } from './Context';

export default function App() {
  const { isAuth } = useContext(Context);
  return (
    <Suspense fallback={<div />}>
      <GlobalStyle />
      <Header />
      <AppRouter isAuth={isAuth} />
      <Navbar />
    </Suspense>
  );
}
